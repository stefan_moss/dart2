using System;
using System.Linq;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using PlayerService.Models;

namespace PlayerService.Data
{
    public static class PrepDb
    {
        public static void PrepPopoulation(IApplicationBuilder app)
        {
            using(var serviceScope = app.ApplicationServices.CreateScope())
            {
                SeedData(serviceScope.ServiceProvider.GetService<AppDbContext>());
            }
        }

        private static void SeedData(AppDbContext context)
        {
            if(!context.Players.Any())
            {
                Console.WriteLine("--> Seeding Data");

                context.Players.AddRange(
                    new Player() {firstName="Stefan", lastName="Moß", shortName="Mozzy"},
                    new Player() {firstName="Marcel", lastName="Voggel", shortName="the Bird"},
                    new Player() {firstName="Joshua", lastName="Blum", shortName="the Cat"},
                    new Player() {firstName="Raphael", lastName="Speh", shortName="Monkey"},
                    new Player() {firstName="Marc", lastName="Eberhart", shortName="Bulley"},
                    new Player() {firstName="Florian", lastName="Allwelt", shortName="26er"}
                );

                context.SaveChanges();
            }
            else
            {
                Console.WriteLine("--> We already have data");
            }
        }
    }
}