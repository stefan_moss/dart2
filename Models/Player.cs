using System.ComponentModel.DataAnnotations;

namespace PlayerService.Models
{
    public class Player
    {
        [Key] 
        [Required]
        public int Id { get; set; }
        [Required]
        public string firstName { get; set; }
        [Required]
        public string lastName { get; set; }
        [Required]
        public string shortName { get; set; }
    }
}