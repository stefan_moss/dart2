using AutoMapper;
using PlayerService.Dtos;
using PlayerService.Models;

namespace PlayerService.Profiles
{
    public class PlayerProfil : Profile
    {
        public PlayerProfil()
        {
            // Source -> Target
            CreateMap<Player, PlayerReadDto>();
            CreateMap<PlayerCreateDto, Player>();
        }
    }
}