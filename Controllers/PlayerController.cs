using System;
using System.Collections.Generic;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using PlayerService.Data;
using PlayerService.Dtos;
using PlayerService.Models;

namespace PlayerService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PlayersController : ControllerBase
    {
        private readonly IPlayerRepo _repository;
        private readonly IMapper _mapper;

        public PlayersController(IPlayerRepo repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper; 
        }

        [HttpGet]
        public ActionResult<IEnumerable<PlayerReadDto>> GetPlayers()
        {
            Console.WriteLine("--> Getting Players...");

            var playerItem = _repository.GetAllPlayers();
            return Ok(_mapper.Map<IEnumerable<PlayerReadDto>>(playerItem));      
        }

        [HttpGet("{id}", Name = "GetPlayerById")]
        public ActionResult<PlayerReadDto> GetPlayerById(int id)
        {
            var playerItem = _repository.GetPlayerById(id);
            if(playerItem != null)
            {
                return Ok(_mapper.Map<PlayerReadDto>(playerItem));
            }

            return NotFound();
        }

        [HttpPost]
        public ActionResult<PlayerReadDto> CreatePlayer(PlayerCreateDto playerCreateDto)
        {
            var playerModel = _mapper.Map<Player>(playerCreateDto);
            _repository.CreatePlayer(playerModel);
            _repository.SaveChanges();

            var playerReadDto = _mapper.Map<PlayerReadDto>(playerModel);

            return CreatedAtRoute(nameof(GetPlayerById), new { Id = playerReadDto.Id}, playerReadDto); 
        }
    }
}