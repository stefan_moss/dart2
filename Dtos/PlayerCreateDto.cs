using System.ComponentModel.DataAnnotations;

namespace PlayerService.Dtos
{
    public class PlayerCreateDto
    {
        [Required]
        public string firstName { get; set; }
        [Required]
        public string lastName { get; set; }
        [Required]
        public string shortName { get; set;} 
    }
}