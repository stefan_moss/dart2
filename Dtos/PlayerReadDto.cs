namespace PlayerService.Dtos
{
    public class PlayerReadDto
    {
        public int Id { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string shortName { get; set; }
    }
}